# this is a fake RT library used for building
# RTFM without having request-tracker3.8 installed

package RT;

$VERSION="3.8.x";
$LocalPath="/usr/share/request-tracker3.8";
$LocalBinPath="/usr/share/doc/rt3.8-rtfm/examples";
$LocalSbinPath="/usr/share/rt3.8-rtfm/sbin";

1;
